VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cEventDelegate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements IEventDelegate
Option Explicit
Private mObj As Object
Private mMethodName As String



Private Sub IEventDelegate_Execute()
    CallByName mObj, mMethodName, VbMethod
End Sub

Private Sub IEventDelegate_SetEvent(ByVal obj As Object, ByVal methodName As String)
    Set mObj = obj
    mMethodName = methodName
End Sub
