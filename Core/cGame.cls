VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cGame"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False


Private WithEvents p_em As cKeyEventManager
Attribute p_em.VB_VarHelpID = -1
Private p_mouse As cMouseEventManager
Private lFinished As Boolean
Private posx As Long
Private posy As Long
Private p_objectPos As cObjectToMove
'

Public Sub init(ByRef em As cKeyEventManager, ByRef emouse As cMouseEventManager, ByRef objectPos)
    Set p_em = em
    Set p_mouse = emouse
    Set p_objectPos = objectPos
End Sub

Public Sub checkEvents()
    p_em.checkForWiredKeys
    
End Sub

Public Sub startGame()


    p_em.resetKeyboard
    p_em.startMe
    p_mouse.reset
    
    lFinished = False
    

End Sub

Public Function finished() As Boolean
    finished = lFinished
End Function

Public Sub stopGame()

    p_em.resetKeyboard
    p_em.stopMe


End Sub


Private Sub p_em_OnHearing(ByVal keyCode As Integer)


    If keyCode = vbKeyLButton Then Stop
    If keyCode = vbKeyEscape Then lFinished = True
    
    Application.DataEntryMode = False
    
    With p_objectPos
        With ActiveSheet.Shapes(1)
            .top = p_objectPos.getPosition(1)
            .Left = p_objectPos.getPosition(0)
        End With
        ActiveCell.Range("a1").Value2 = .getPosition(0) & " " & .getPosition(1)
    End With
    
    Application.DataEntryMode = True
    
End Sub
