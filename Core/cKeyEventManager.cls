VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cKeyEventManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private Declare Function GetAsyncKeyState Lib "user32.dll" (ByVal vKey As Long) As Integer
Private Declare Function VkKeyScan Lib "user32" Alias "VkKeyScanA" (ByVal cChar As Byte) As Integer
Private Declare Function GetKeyboardState Lib "user32.dll" (ByRef out As Byte) As Long
Private Declare Function SetKeyboardState Lib "user32" (lppbKeyState As Byte) As Long

Public Event OnHearing(ByVal keyCode As Integer)
Public Event OnStartEventManager()
Public Event OnStopEventManager()

Private listener(255)  As Boolean
Private eventDelegates(255) As IEventDelegate
Private keys(255) As Byte
Private amListening As Boolean

Public Function isListening()
    isListening = amListening
End Function
Public Sub addListenFor(ByVal keyCode As Long, Optional ByRef eventDelegate As IEventDelegate = Nothing)
    listener(keyCode) = True
    Set eventDelegates(keyCode) = eventDelegate
End Sub

Public Function isListeningFor(ByVal keyCode As Long)
        isListeningFor = listener(keyCode)
End Function

Public Function getCurrentKeyboard(Optional ByVal keyCode As Long = 0) As Variant
    
    GetKeyboardState keys(0)

    If keyCode = 0 Then
        getCurrentKeyboard = keys
        Exit Function
    End If

    getCurrentKeyboard = keys(keyCode)
    
End Function

Public Function hasBeenHeard(ByVal keyCode As Long) As Boolean
    Dim keyState As Integer
  
    GetKeyboardState keys(0)
    
    hasBeenHeard = keys(keyCode) > 0
    
End Function

Public Sub checkForWiredKeys()
    Dim i As Long
    GetKeyboardState keys(0)
    
    For i = 0 To 255
        If keys(i) <> 0 And listener(i) Then
            If Not eventDelegates(i) Is Nothing Then
                eventDelegates(i).Execute
            End If
            RaiseEvent OnHearing(i)
        End If
    Next i
    Me.resetKeyboard
End Sub
Public Sub resetKeyboard(Optional ByVal keyCode As Long = 0)
    
    Erase keys
    
    SetKeyboardState keys(0)

End Sub

Public Sub startMe()
    RaiseEvent OnStartEventManager
End Sub

Public Sub stopMe()
    RaiseEvent OnStopEventManager
End Sub

Public Sub OnStartEventManager()
   
End Sub
Public Sub OnStopEventManager()
    
End Sub
