VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cKeyInput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Declare Function SendInput Lib "user32.dll" (ByVal nInputs As Long, ByRef pInputs As Any, ByVal cbSize As Long) As Long


Private Const INPUT_MOUSE As Long = 0
Private Const INPUT_KEYBOARD As Long = 1
Private Const KEYEVENTF_KEYUP As Long = 2
Private Const VK_LSHIFT = &HA0
'
Public Sub sendKeyInput(ByVal val As String)
    Dim ki() As keyInput
    ki = getKeyInput(val)
    Call SendInput(1, ki(0), LenB(ki(0)))
    DoEvents
End Sub


Public Function getKeyInput(ByVal val As String) As keyInput()
    
    Dim ki(4) As keyInput
    
    
     ki(0).dwType = INPUT_KEYBOARD 'shift down
     ki(0).wVK = val
     ki(1) = ki(0) ' key up
     ki(1).dwFlags = KEYEVENTF_KEYUP
     
     getKeyInput = ki
     
End Function

