VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cMouseEventManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetCursorPos Lib "user32" (ByRef pos As lpPoint) As Boolean

Private m_p As lpPoint

Public Function origin() As Variant
    origin = Array(m_p.x, m_p.y)
End Function
    

Public Sub reset()
    
    Call GetCursorPos(m_p)
    
End Sub

Public Function getMouseDelta() As Variant

    Dim l_p As lpPoint
    
    GetCursorPos l_p
    l_p.x = l_p.x - m_p.x
    l_p.y = l_p.y - m_p.y
    
    
    getMouseDelta = Array(l_p.x, l_p.y)

End Function

