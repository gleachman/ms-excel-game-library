VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cObjectToMove"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False


Private m_pos

    Public Function setPosition(ByVal v As Variant)
        m_pos = v
    End Function

    Public Function getPosition() As Variant
        getPosition = m_pos
    End Function
    

    Public Sub moveUp()
        m_pos(1) = m_pos(1) - 10
    End Sub
    Public Sub moveDown()
        m_pos(1) = m_pos(1) + 10
    End Sub
    Public Sub moveLeft()
        m_pos(0) = m_pos(0) - 10
    End Sub
    Public Sub moveRight()
        m_pos(0) = m_pos(0) + 10
    End Sub
