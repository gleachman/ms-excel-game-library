Attribute VB_Name = "factory"
Option Explicit

Public Function getKeyEventManager() As cKeyEventManager

    Set getKeyEventManager = New cKeyEventManager
    
End Function
Public Function getMouseEventManager() As cMouseEventManager
    Set getMouseEventManager = New cMouseEventManager
End Function

Public Function getKeyInput()
    Set getKeyInput = New cKeyInput
End Function

Public Function getMouseInput()
    Set getMouseInput = New cMouseInput
End Function

Public Function createEventDelegate(ByRef obj As Object, ByVal methodName As String) As IEventDelegate
    Dim anEventDelegate As IEventDelegate
    Set anEventDelegate = New cEventDelegate
    Call anEventDelegate.SetEvent(obj, methodName)
    Set createEventDelegate = anEventDelegate
End Function
