Attribute VB_Name = "mGame"
Public Sub start()

   Dim gm As New cGame
   
   Dim eh As New cEventHookup
   
    Dim em As New cKeyEventManager
    
    Dim emouse As New cMouseEventManager
    Dim objectToMove As New cObjectToMove
    
    objectToMove.setPosition (Array(100, 100))
    
    Call em.addListenFor(vbKeyLeft, factory.createEventDelegate(objectToMove, "moveLeft"))
    Call em.addListenFor(vbKeyRight, factory.createEventDelegate(objectToMove, "moveRight"))
    Call em.addListenFor(vbKeyDown, factory.createEventDelegate(objectToMove, "moveDown"))
    Call em.addListenFor(vbKeyUp, factory.createEventDelegate(objectToMove, "moveUp"))
    
    Call em.addListenFor(vbKeyEscape)
    
    Call em.addListenFor(vbKeyLButton)
    
    emouse.reset

   Call gm.init(em, emouse, objectToMove)
   gm.startGame
   
   While Not gm.finished
        
        gm.checkEvents
        
               
        DoEvents
   
   Wend
   
   gm.stopGame
   
    
End Sub


