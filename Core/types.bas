Attribute VB_Name = "types"
Public Type keyInput      '   typedef struct tagINPUT {
 dwType As Long                '     DWORD type;
 wVK As Integer                '     union {MOUSEINPUT mi;
 wScan As Integer              '               KEYBDINPUT ki;
 dwFlags As Long               '               HARDWAREINPUT hi;
 dwTime As Long                '              };
 dwExtraInfo As Long           '     }INPUT, *PINPUT;
 dwPadding As Currency         '   8 extra bytes, because mouse take more.
End Type

Public Type lpPoint
    x As Long
    y As Long
End Type
