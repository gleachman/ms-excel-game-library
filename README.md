##MS Excel Game Library

###What is this?
This project is an exercise in various elements of coding.

 - I intend writing all this code with strict TDD
    - You are not allowed to write any production code unless it is to make a failing unit test pass.
    - You are not allowed to write any more of a unit test than is sufficient to fail; and compilation failures are failures.
    - You are not allowed to write any more production code than is sufficient to pass the one failing unit test.
 - I will be implementing lots of interfaces
    - this will allow easy mocking.
 - I will be writing an vba/vb6 unittest framework


###How the project should be tested

 - Create 2 Excel workbooks 
 - Core
 - Test
 - Import all the code files from Core folder into the Core workbook.
 - Import all the code files from Test folder into the Test workbook.
 - reference the Core workbook from the Test workbook.
 - Save all the books.
 - Press F5, choose the test workbook and choose the appropriate testGroup to run.
 - This process will improve once I have importing all modules, and a testframework built.

###Tools

 - use of [MZ Tools for VB][1]
 - use of [online Markdown editor][2]

[1]: http://www.mztools.com/v3/download.aspx
[2]: http://www.ctrlshift.net/project/markdowneditor/