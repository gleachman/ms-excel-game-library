VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cTestEventDelegate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements ITestAble
Implements IEventDelegate
Option Explicit

Public t As ITestRunner
Private sut As IEventDelegate

Private events
Private foundEvents
Private anEvent
Private i As Long
Private hasRun As Boolean
Private obj As Object
Private methodName As String
'
Public Sub setTrue()
    hasRun = True
End Sub
Private Sub IEventDelegate_Execute()
    CallByName obj, methodName, VbMethod
End Sub

Private Sub IEventDelegate_SetEvent(ByVal obj As Object, ByVal methodName As String)
    Me.obj = obj
    Me.methodName = methodName
End Sub

Private Function ITestAble_className() As String
    ITestAble_className = "cTestEventDelegate"
End Function

Private Sub ITestAble_setTestRunner(testRunner As unittestFramework.ITestRunner)
    Set t = testRunner
End Sub

Private Sub ITestAble_setUp()
    Set sut = factory.createEventDelegate(Me, "setTrue")
End Sub

Private Sub ITestAble_tearDown()

End Sub

Private Sub ITestAble_testall()

    Dim tests As Variant
    tests = Array( _
        "test_whenExecuteCalled")
    
    t.runTests Me, tests
    
End Sub

Public Sub test_whenExecuteCalled()

    hasRun = False
    Call sut.Execute
    Call t.AssertTrue(hasRun, "should run the event created")
    
End Sub
