VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cTestEventManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements ITestAble
Option Explicit

    Public t As ITestRunner
Attribute t.VB_VarHelpID = -1
    Private WithEvents sut As cKeyEventManager
Attribute sut.VB_VarHelpID = -1
    
    Private events
    Private foundEvents
    Private anEvent
    Private i As Long

Private Function ITestAble_className() As String
    ITestAble_className = "cTestEventManager"
End Function

Private Sub ITestAble_setTestRunner(testRunner As ITestRunner)
    Set t = testRunner
End Sub

Private Sub ITestAble_setUp()
    Set sut = factory.getKeyEventManager
End Sub

Private Sub ITestAble_tearDown()
    Set sut = Nothing
End Sub

Private Sub ITestAble_testall()

    Dim tests As Variant
    tests = Array( _
        "test_eventManager_add_listen_for", _
        "test_eventManagerIsListeningFor", _
        "test_eventManagerIsListeningFor", _
        "test_eventManagerHasHeard", _
        "test_eventManagerHasHeardEvenAfterOtherKey", _
        "test_eventManagerReset", _
        "test_getKeyInput", _
        "test_getCurrentKeyboardReturnsTheKeysPressed", _
        "test_getCurrentKeyboardReturnsAllKeysPressedSinceLastReset", _
        "test_IsListening", _
        "test_WhenEventIsHeardAndEventDelegateIsSet")
    
    t.runTests Me, tests
    
End Sub
Public Sub test_eventManager_add_listen_for()
    
    Call sut.addListenFor(vbKeyLeft)
    
    t.AssertTrue True
    
End Sub



Public Sub test_eventManagerIsListeningFor()

    Dim res As Boolean
    
    sut.addListenFor (vbKeyLeft)
    res = sut.isListeningFor(vbKeyLeft)
    
    t.AssertEqual res, True, "Is listening for should return 1"
    
End Sub

Public Sub test_eventManagerHasHeard()
    
    Dim res As Boolean
    Dim ki() As keyinput
    Dim keyinput As cKeyInput
    
    Set keyinput = factory.getKeyInput
    
    Call sut.resetKeyboard
    res = sut.hasBeenHeard(vbKeyEscape)
    
    t.AssertTrue Not res, "event manager should not have heard key press yet"
    
    Call keyinput.sendKeyInput(vbKeyEscape)
    res = sut.hasBeenHeard(vbKeyEscape)
    
    t.AssertTrue res, "Key has been pressed, but event Manager hasn't heard it"
    
End Sub

Public Sub test_eventManagerHasHeardEvenAfterOtherKey()
    
    
    Dim res As Boolean
    Dim ki() As keyinput
    Dim createKey As cKeyInput
    
    Set createKey = factory.getKeyInput
    
    
    sut.resetKeyboard
    
    res = sut.hasBeenHeard(vbKeyEscape)
    
    t.AssertTrue Not res, "escape key should not have been heard yet"
    
    Call createKey.sendKeyInput(vbKeyEscape)
    Call createKey.sendKeyInput(vbKeyLeft)
    Call createKey.sendKeyInput(vbKeyRight)
    
    res = sut.hasBeenHeard(vbKeyEscape)
    
    t.AssertTrue res, "escape key has been pressed and should have been heard"
    
End Sub

Public Sub test_eventManagerReset()

    
    Dim res As Boolean
    Dim ki() As keyinput
    Dim createInput As cKeyInput
    Set createInput = factory.getKeyInput
    
    
    Call createInput.sendKeyInput(vbKeyEscape)
    Call createInput.sendKeyInput(vbKeyLeft)
    Call createInput.sendKeyInput(vbKeyRight)
    Call createInput.sendKeyInput(vbKeyUp)
    
    res = sut.hasBeenHeard(vbKeyEscape)
    t.AssertTrue res, "escape key should have been heard"
    res = sut.hasBeenHeard(vbKeyRight)
    t.AssertTrue res, "right key should have been heard"
    
    Call sut.resetKeyboard
    
    res = sut.hasBeenHeard(vbKeyEscape)
    t.AssertTrue Not res, "keys have been reset, escape should not have been heard"
    
End Sub

Public Sub test_getKeyInput()

    Dim res() As keyinput
    Dim createInput As cKeyInput
    Set createInput = factory.getKeyInput
    
    res = createInput.getKeyInput(vbKeyEscape)
    
    t.AssertEqual res(0).dwType, 1, "keyInput should be pressed"
    
End Sub


Public Sub test_getCurrentKeyboardReturnsTheKeysPressed()
    
    Dim k As Variant
    Dim v As Byte
    Dim res As Boolean
    Dim akey
    Dim createInput As cKeyInput
    
    Set createInput = factory.getKeyInput
    
    k = Array(vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown, vbKeyEscape)
    
    For Each akey In k
        
        sut.resetKeyboard
        Call createInput.sendKeyInput(akey)
        
        v = CByte(sut.getCurrentKeyboard(akey))
        res = (v > 0)
        t.AssertTrue res, akey & " has been pressed but keyboard is not finding it"
        
    Next akey
    
End Sub

Public Sub test_getCurrentKeyboardReturnsAllKeysPressedSinceLastReset()
    
    Dim keys(255)
    Dim keysToPress As Variant
    Dim v As Variant
    Dim akey
    Dim createInput As cKeyInput
    
    Set createInput = factory.getKeyInput
    
    keysToPress = Array(vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown, vbKeyEscape)
    
    sut.resetKeyboard
    
    For Each akey In keysToPress
        keys(akey) = 129
        Call createInput.sendKeyInput(akey)
        DoEvents
    Next akey
    v = sut.getCurrentKeyboard()
    t.AssertArrayEqual v, keys, "arrays are not equal"
    
    
End Sub

Public Sub test_IsListening()
    
    Dim akey
    Dim keyinput As cKeyInput
    
    Set keyinput = factory.getKeyInput
    
    Application.DataEntryMode = True
    sut.resetKeyboard
    Dim i As Long
    events = Array(vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown, vbKeyEscape)
    
    For Each akey In events
        Call sut.addListenFor(akey)
    Next akey
    
    For Each akey In events
        Call keyinput.sendKeyInput(akey)
    Next akey
    
    ReDim foundEvents(UBound(events))
    i = 0
    sut.checkForWiredKeys
    
    t.AssertEqual sum(foundEvents), sum(events), "Some of the events have not been found"
    
    Application.DataEntryMode = False
End Sub


Public Function test_WhenEventIsHeardAndEventDelegateIsSet()
    
    
    Dim keyinput As cKeyInput
    Dim akey As Variant
    
    anEvent = Array(0, 0, 0, 0)
    
    Call sut.addListenFor(vbKeyLeft, factory.createEventDelegate(Me, "delegateMethodForTestLeft"))
    Call sut.addListenFor(vbKeyRight, factory.createEventDelegate(Me, "delegateMethodForTestRight"))
    Call sut.addListenFor(vbKeyUp, factory.createEventDelegate(Me, "delegateMethodForTestUp"))
    Call sut.addListenFor(vbKeyDown, factory.createEventDelegate(Me, "delegateMethodForTestDown"))

    Set keyinput = factory.getKeyInput
    For Each akey In Array(vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown, vbKeyEscape)
        Call keyinput.sendKeyInput(akey)
    Next akey
    
    ReDim foundEvents(10)
    sut.checkForWiredKeys

    Call t.AssertArrayEqual(anEvent, Array("Left", "Right", "Up", "Down"), "should execute all the delegate events")
    
End Function

Public Sub delegateMethodForTestLeft()
    anEvent(0) = "Left"
End Sub
Public Sub delegateMethodForTestRight()
    anEvent(1) = "Right"
End Sub
Public Sub delegateMethodForTestUp()
    anEvent(2) = "Up"
End Sub
Public Sub delegateMethodForTestDown()
    anEvent(3) = "Down"
End Sub
    
Private Function sum(ByVal ev As Variant)
    Dim i As Long
    Dim tot As Long
    For i = 0 To UBound(ev)
        tot = tot + ev(i)
    Next i
    sum = tot
End Function


Private Sub sut_OnHearing(ByVal keyCode As Integer)
    
    foundEvents(i) = keyCode
    i = i + 1
        
End Sub

