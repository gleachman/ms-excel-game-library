VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cTestMouseEventManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Implements ITestAble

Public t As ITestRunner

Private evm As cMouseEventManager
Private mi As cMouseInput

Private events
Private foundEvents
Private anEvent
Private i As Long


Private Function ITestAble_className() As String
    ITestAble_className = "cTestMouseEventManager"
End Function

Private Sub ITestAble_setTestRunner(testRunner As ITestRunner)
    Set t = testRunner
End Sub

Private Sub ITestAble_setUp()
    Set evm = factory.getMouseEventManager
    evm.Reset
    Set mi = factory.getMouseInput

End Sub

Private Sub ITestAble_tearDown()

End Sub


Private Sub ITestAble_testall()
    

    t.runTests Me, Array("test_WhenResetCalled", _
                         "test_MouseDeltaCalledAfterMouseMoved")

End Sub

Public Sub test_MouseDeltaCalledAfterMouseMoved()

    Dim mi As cMouseInput
    Dim mouseDelta As Variant
    Set mi = factory.getMouseInput
    Call mi.MoveMouse(100, 100)

    Call evm.Reset

    'Act
    Call mi.MoveMouse(150, 50)
    mouseDelta = evm.getMouseDelta

    'Assert

    t.AssertArrayEqual mouseDelta, Array(50, -50), "should show x,y of mouse moved"
    
End Sub
Public Sub test_WhenResetCalled()
    
    Dim lp As Variant
    
    Call mi.MoveMouse(100, 100)
    evm.Reset
    lp = evm.origin
    
    Call t.AssertArrayEqual(lp, Array(100, 100), "Should return origin position")
    
End Sub

