Attribute VB_Name = "mTestClasses"
Option Explicit

Private t As ITestRunner

Private Sub runTestsForClass(ByRef acls As ITestAble)
    
    Call acls.setTestRunner(t)
    Debug.Print vbCr & acls.className
    acls.testAll
    
End Sub

Public Sub auto_open()
    Set t = unittest.tester
    Call runTestsForClass(New cTestEventManager)
    Call runTestsForClass(New cTestMouseEventManager)
    Call runTestsForClass(New cTestEventDelegate)
End Sub


